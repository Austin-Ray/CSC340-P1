package main;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import model.Vector;

public final class Main {
  public static void main(String[] args) {

  }

  public static ArrayList<Vector> readData(File file) {
    return readData(file.getAbsoluteFile());
  }

  public static ArrayList<Vector> readData(String infile) {
    ArrayList<Vector> vectors = new ArrayList<>();
    BufferedReader br;

    System.out.println(infile);
    try {
      System.out.println(new File("").getCanonicalPath());
      br = new BufferedReader(new FileReader(infile));
      // Read the header line
      br.readLine();
      String line;
      while((line = br.readLine()) != null) {
        String[] splitLn = line.split("\t");
        Double[] dblArr = new Double[splitLn.length];

        for(int i = 0; i < splitLn.length; i++) {
          dblArr[i] = Double.parseDouble(splitLn[i]);
        }

        Vector newVector = new Vector(dblArr[0], dblArr[1], dblArr[2], dblArr[3]);
        vectors.add(newVector);
      }
    } catch (IOException e) {
      e.printStackTrace();
    }

    return vectors;
  }
}
