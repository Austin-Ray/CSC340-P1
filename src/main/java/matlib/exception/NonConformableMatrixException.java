package matlib.exception;

public class NonConformableMatrixException extends Exception {
    public NonConformableMatrixException() {}
    public NonConformableMatrixException(String msg) {
        super(msg);
    }
}
